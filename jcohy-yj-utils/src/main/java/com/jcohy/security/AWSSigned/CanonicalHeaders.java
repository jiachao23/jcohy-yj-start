package com.jcohy.security.AWSSigned;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/20 16:05.
 * ClassName  : CanonicalHeaders
 * Description  :
 */
public class CanonicalHeaders {

    private String key;

    private String value;

    public CanonicalHeaders(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
