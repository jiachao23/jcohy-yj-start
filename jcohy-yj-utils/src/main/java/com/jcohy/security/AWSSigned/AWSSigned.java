package com.jcohy.security.AWSSigned;

import com.jcohy.lang.EncodeConvertUtils;
import com.jcohy.lang.StringUtils;
import com.jcohy.security.SecurityUtils;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.List;
import java.util.Map;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/20 15:11.
 * ClassName  : AWSSigned
 * Description  :
 */
public class AWSSigned {

    private List<Map<String,String>> awsRequest ;

    private  String AWSAccessKeyId ;

    private  String AWSSecretAccessKey;

    public AWSSigned(String AWSAccessKeyId,String AWSSecretAccessKey) {
        this.AWSAccessKeyId = AWSAccessKeyId;
        this.AWSSecretAccessKey = AWSSecretAccessKey;
    }

    public AWSSigned() {
    }

    /**
     * 规范请求
     * @param awsRequest
     */
    public  String canonicalrRequest(AWSRequest awsRequest){
        StringBuilder sb = new StringBuilder();

        sb.append(awsRequest.getRequestMethod()+"\n")
                .append(awsRequest.getCanonicalUri()+"\n")
                .append(awsRequest.getCanonicalQueryString()+"\n");
        //content-type:application/x-www-form-urlencoded; charset=utf-8
        for(CanonicalHeaders canonicalHeaders :awsRequest.getCanonicalHeaders()){
            sb.append(canonicalHeaders.getKey()+":"+canonicalHeaders.getValue()+"\n");
        }
        sb.append("\n");
        sb.append(awsRequest.getSignedHeaders()+"\n");
        sb.append(awsRequest.getHexEncode());
        return sb.toString();
    }

    public  String hashSHA256(String str){
        return SecurityUtils.sha256(str);
    }

    public  String createStringToSign(StringToSign stringToSign){
        StringBuilder sb = new StringBuilder();
        sb.append(stringToSign.getAlgorithm()+"\n")
                .append(stringToSign.getRequestDateTime()+"\n")
                .append(stringToSign.getCredentialScope()+"\n")
                .append(stringToSign.getHashedCanonicalRequest());
        return sb.toString();

    }

    public  byte[] HmacSHA256( byte[] key,String data) throws Exception {
        String algorithm="HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        byte[] aFinal = mac.doFinal(data.getBytes("UTF8"));

        System.out.println(EncodeConvertUtils.toHex(aFinal));
        return aFinal;
    }

    public  byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception {
        byte[] kSecret = ("AWS4" + key).getBytes("UTF8");
        byte[] kDate = HmacSHA256( kSecret,dateStamp);
        byte[] kRegion = HmacSHA256(kDate,regionName);
        byte[] kService = HmacSHA256(kRegion,serviceName );
        byte[] kSigning = HmacSHA256(kService,"aws4_request");
        return kSigning;
    }

    public  char[] createSignaturekey(AWSRequest awsRequest,StringToSign stringToSign){
        char[] result = null;
        try {
            if(StringUtils.isEmpty(awsRequest.getHexEncode())){
                awsRequest.setHexEncode("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
            }
            //1.规范请求
            System.out.println("----------------------------------------------------开始规范请求:打印规范请求---------------------------------");
            String canonicalrRequest = canonicalrRequest(awsRequest);
            System.out.println(canonicalrRequest);
            System.out.println("----------------------------------------------------结束规范请求---------------------------------------------");

            System.out.println();

            //2、计算请求的哈希值、这里默认为SHA256
            System.out.println("----------------------------------------------------开始计算规范请求的hash值，打印hash值----------------------");
            String hashSHA256 = hashSHA256(canonicalrRequest);
            //将哈细值付给待签名的字符串。
            stringToSign.setHashedCanonicalRequest(hashSHA256);
            System.out.println(hashSHA256);
            System.out.println("----------------------------------------------------开始计算规范请求的hash值，打印hash值----------------------");

            System.out.println();

            //3.创建要签名的字符串
            System.out.println("----------------------------------------------------开始创建要签名的字符串：打印待加密字符串-------------------");
            String sign = createStringToSign(stringToSign);
            System.out.println(sign);
            System.out.println("----------------------------------------------------结束创建要签名的字符串------------------------------------");

            System.out.println();

            //4.创建派生密钥
            System.out.println("----------------------------------------------------开始Signature创建派生密钥：打印密钥-----------------------");
            String date = stringToSign.getRequestDateTime().substring(0,8);
            byte[] signatureKey = getSignatureKey(AWSSecretAccessKey, date, stringToSign.getRegionName()
                    , stringToSign.getServiceName());
            char[] chars = Hex.encodeHex(signatureKey);
            System.out.println(chars);
            System.out.println("----------------------------------------------------结束Signature创建派生密钥-------------------------------");

            System.out.println();

            System.out.println("----------------------------------------------------计算签名：（打印签名）-----------------------------------");
//            byte[] hmacSHA256 =HmacSHA256(sign, signatureKey);
//            result = Hex.encodeHex(hmacSHA256);

            System.out.println(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
