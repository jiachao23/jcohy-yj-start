package com.jcohy;

import com.jcohy.mail.Mail;
import com.jcohy.mail.MailUtils;

import javax.mail.MessagingException;
import javax.mail.Session;
import java.io.IOException;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/6 15:24.
 * ClassName  : Testmail
 * Description  :
 */

public class Testmail {

    public static void main(String[] args) throws IOException, MessagingException {
        Session session = MailUtils.createSession("hwsmtp.qiye.163.com", "jiac@startimes.com.cn", "Jia19931203");
        Mail mail = new Mail("jiac@startimes.com.cn","dongch@startimes.com.cn","1111","2222");
        MailUtils.send(session,mail);
    }
}
