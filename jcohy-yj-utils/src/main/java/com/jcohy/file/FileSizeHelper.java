package com.jcohy.file;



import java.text.DecimalFormat;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2017/11/11 13:29.
 * ClassName  : FileSizeHelper
 * Description  :
 */
public class FileSizeHelper {

    private static long ONE_B=1;

    private static long ONE_KB = ONE_B*1024;

    private static long ONE_MB = ONE_KB * 1024;

    public static long ONE_GB = ONE_MB * 1024;

    public static long ONE_TB = ONE_GB * (long)1024;

    public static long ONE_PB = ONE_TB * (long)1024;

    private String[] UNIT_NAME = new String[]{"PB","TB","GB","MB","KB","B"};

    public static String getHumanReadableFileSize(long fileSize) {
        if(fileSize < 0) {
            return String.valueOf(fileSize);
        }
        String result = getHumanReadableFileSize(fileSize, ONE_PB, "PB");
        if(result != null) {
            return result;
        }
        result = getHumanReadableFileSize(fileSize, ONE_TB, "TB");
        if(result != null) {
            return result;
        }
        result = getHumanReadableFileSize(fileSize, ONE_GB, "GB");
        if(result != null) {
            return result;
        }
        result = getHumanReadableFileSize(fileSize, ONE_MB, "MB");
        if(result != null) {
            return result;
        }
        result = getHumanReadableFileSize(fileSize, ONE_KB, "KB");
        if(result != null) {
            return result;
        }
        return String.valueOf(fileSize)+"B";
    }

    private static String getHumanReadableFileSize(long fileSize, long unit, String unitName) {
        if(fileSize == 0){
            return "0";
        }

        if(fileSize / unit >= 1) {
            double value = fileSize / (double)unit;
            DecimalFormat df = new DecimalFormat("######.##"+unitName);
            return df.format(value);
        }
        return null;
    }

    private static long getFileSizeByte(String fileSize,long unit,String unitName){
        long size = Long.valueOf(fileSize.substring(0,fileSize.lastIndexOf(unitName)));
        return size*unit;
    }
    private static String getFileUnit(String filesize){
        if(filesize.endsWith("BB")){
            return "B";
        }else{
            return filesize.substring(filesize.length()-2,filesize.length());
        }
    }
    public static long getFileSizeByte(String filesize){
        String unitName= getFileUnit(filesize);

        if(unitName.equalsIgnoreCase("PB")){
            return getFileSizeByte(filesize,ONE_PB,"PB");
        }
        if(unitName.equalsIgnoreCase("TB")){
            return getFileSizeByte(filesize,ONE_TB,"TB");
        }
        if(unitName.equalsIgnoreCase("GB")){
            return getFileSizeByte(filesize,ONE_GB,"GB");
        }
        if(unitName.equalsIgnoreCase("MB")){
            return getFileSizeByte(filesize,ONE_MB,"MB");
        }
        if(unitName.equalsIgnoreCase("KB")){
            return getFileSizeByte(filesize,ONE_KB,"KB");
        }
        return getFileSizeByte(filesize,ONE_B,"BB");
    }
}
