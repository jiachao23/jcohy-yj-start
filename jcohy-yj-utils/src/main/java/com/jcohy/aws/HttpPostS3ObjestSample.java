package com.jcohy.aws;

import com.jcohy.aws.auth.AWS4SignerBase;
import com.jcohy.aws.auth.AWS4SignerForAuthorizationHeader;
import com.jcohy.http.HttpUtils;
import com.jcohy.lang.EncodeConvertUtils;
import com.jcohy.lang.StringUtils;
import com.jcohy.security.AWSSigned.AWSSigned;
import com.jcohy.security.AWSSigned.sss;
import com.jcohy.security.SecurityUtils;
import org.apache.commons.codec.binary.Hex;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/21 10:53.
 * ClassName  : HttpPostS3ObjestSample
 * Description  :
 */
public class HttpPostS3ObjestSample {

    public static void calculator(String policy,String bucketName, String date,String regionName, String awsAccessKey, String awsSecretKey) throws Exception {
        System.out.println("************************************************");
        System.out.println("*             Calculator Signature             *");
        System.out.println("************************************************");

        System.out.println("--------- SignerToString ---------");
        System.out.println("------------------------------------");
//        URL endpointUrl;
//        try {
//            endpointUrl = new URL("https://" + bucketName + ".s3.amazonaws.com/"+key);
//        } catch (MalformedURLException e) {
//            throw new RuntimeException("Unable to parse service endpoint: " + e.getMessage());
//        }

//        Map<String, String> headers = new HashMap<String, String>();
//
        String str = StringUtils.str(policy.getBytes(), "UTF-8");
        System.out.println("--------------签名参数----------------------");
        AWSSigned awsSigned = new AWSSigned();
        System.out.println(str);
        System.out.println(awsSecretKey);
        System.out.println(date);
        System.out.println(regionName);
        System.out.println("s3");
        System.out.println("------------------------------------");

//        byte[] signatureKey = awsSigned.getSignatureKey(awsSecretKey, date, regionName, "s3");
        byte[] signatureKey = sss.getSignatureKey("wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY", "20151229", "us-east-1", "s3");

        System.out.println("-----------------signatureKey-------------------");
        System.out.println(EncodeConvertUtils.toHex(signatureKey));
        System.out.println("------------------------------------");

//        String sha256 = SecurityUtils.sha256(str);


        byte[] s  = sss.HmacSHA256(str,signatureKey);
        String s1 = EncodeConvertUtils.toHex(s);
        System.out.println("-----------------sign-------------------");
        System.out.println(s1);
        System.out.println( Hex.encodeHex("ef8c088fcdfdba23f32224f699875e3d739ebf05cf12979bb34e10d977010663".getBytes()));

        // make the call to Amazon S3
        System.out.println("------------------------------------");
    }
}
