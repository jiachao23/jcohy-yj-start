## Jcohy-yj

整理，修改一些开源类库，将常用的工具操作类汇总。

 | [官网](http://www.jcohy.com) 
 | [github](https://github.com/jiachao23/jcohy-yj-start)

## 版本

v1.1.0.2

> 新增ArrayUtils,StringUtils,DateUtils,FileSizeHelper,SystemUtils,MailUtils,SecurityUtils。

v1.1.0.1

> 新增jcohy-yj-utils模块。

### License

    Copyright 2018 jcohy jiac<jia_chao@126.com>
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
      http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.