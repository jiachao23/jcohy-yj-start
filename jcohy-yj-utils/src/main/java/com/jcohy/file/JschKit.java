package com.jcohy.file;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import java.util.Vector;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/11/11 11:56.
 * ClassName  : JschKit
 * Description  :
 */
public class JschKit {

    private static SFTPChannel sftpChannel;
    private static ChannelSftp channelSftp;


    public JschKit(SFTPChannel sftpChannel) {
        if(sftpChannel == null){
            JschKit.sftpChannel = new SFTPChannel();

        }
        JschKit.sftpChannel = sftpChannel;
    }

    public static Vector getdirs(String dstDirPath) throws JSchException, SftpException {
        channelSftp =sftpChannel.getChannel(SFTPConstants.getDefaultSftp());
        Vector vector = channelSftp.ls(dstDirPath);
        return vector;
    }


}
