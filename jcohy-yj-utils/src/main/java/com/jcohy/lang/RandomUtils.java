package com.jcohy.lang;

import java.util.UUID;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/5 10:47.
 * ClassName  : Random
 * Description  :
 */
public class RandomUtils {

    /**
     * 清爽uuid，无中间线
     *
     * @return result
     */
    public static String getUuidCool() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 完整uuid，带中间线
     *
     * @return result
     */
    public static String getUuid() {
        return UUID.randomUUID().toString();
    }
}
