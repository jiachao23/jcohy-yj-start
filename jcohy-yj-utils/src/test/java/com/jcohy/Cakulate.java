package com.jcohy;

import com.jcohy.security.AWSSigned.AWSRequest;
import com.jcohy.security.AWSSigned.AWSSigned;
import com.jcohy.security.AWSSigned.CanonicalHeaders;
import com.jcohy.security.AWSSigned.StringToSign;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/20 10:05.
 * ClassName  : Cakulate
 * Description  :
 */
public class Cakulate {


    public static void main(String[] agrs) throws Exception {
        AWSRequest awsRequest = new AWSRequest();

        awsRequest.setRequestMethod("GET");
        awsRequest.setCanonicalUri("/");
        awsRequest.setCanonicalQueryString("Action=ListUsers&Version=2010-05-08");

        List<CanonicalHeaders> queryString = new ArrayList<>();
        CanonicalHeaders q1 = new CanonicalHeaders("content-type","application/x-www-form-urlencoded; charset=utf-8");
        CanonicalHeaders q2 = new CanonicalHeaders("host","iam.amazonaws.com");
        CanonicalHeaders q3 = new CanonicalHeaders("x-amz-date","20150830T123600Z");
        queryString.add(q1);
        queryString.add(q2);
        queryString.add(q3);

        awsRequest.setCanonicalHeaders(queryString);

        awsRequest.setSignedHeaders(q1.getKey()+";"+q2.getKey()+";"+q3.getKey());

        StringToSign stringToSign = new StringToSign();
        stringToSign.setAlgorithm("AWS4-HMAC-SHA256");
        stringToSign.setCredentialScope("20150830/us-east-1/iam/aws4_request");
        stringToSign.setRequestDateTime("20150830T123600Z");
        stringToSign.setRegionName("us-east-1");
        stringToSign.setServiceName("iam");

        AWSSigned awsSigned = new AWSSigned("AKIAIOSFODNN7EXAMPLE","wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY");
        awsSigned.createSignaturekey(awsRequest,stringToSign);


    }
}
