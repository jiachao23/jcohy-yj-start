package com.jcohy.file;

import com.jcraft.jsch.SftpProgressMonitor;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/11/11 11:21.
 * ClassName  : MyProgressMonitor
 * Description  :
 */
public class MyProgressMonitor implements SftpProgressMonitor {

    private double transfered=0;
    private double max=0;
    private String percent;
//    private static final Logger logger = LoggerFactory.getLogger(MyProgressMonitor.class);
    public MyProgressMonitor(double max) {
        // TODO Auto-generated constructor stub
        this.max = max;
    }
    @Override
    public boolean count(long count) {
        transfered = transfered + count;
        DecimalFormat format = new DecimalFormat("#0.00");
        percent = format.format(transfered/max*100);
        return true;
    }

    @Override
    public void end() {
//        logger.info("Transferring done.");
//        System.out.println("Transferring done.");
    }

    @Override
    public void init(int op, String src, String dest, long max) {
//        logger.info("Transferring begin.");
//        System.out.println("Transferring begin.");
    }

    public String getPercent() {
        return percent;
    }

    public String getTransfered() {
        return String.valueOf(transfered);
    }
}
