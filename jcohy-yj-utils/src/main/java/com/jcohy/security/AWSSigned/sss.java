package com.jcohy.security.AWSSigned;

import com.jcohy.lang.EncodeConvertUtils;
import org.apache.commons.codec.binary.Hex;
import sun.misc.BASE64Encoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jiac on 2018/3/23.
 * ClassName  : com.jcohy.security.AWSSigned
 * Description  :
 */
public class sss {

    public static String aws_secret_key = "KTicGu8xXXSXxqvwhP2GZbHR9oxWF3Kft3NcdbTU";
//    public static String aws_secret_key = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY";
    public sss() throws Exception {
    }

    public static byte[] HmacSHA256(String data, byte[] key) throws Exception {
        String algorithm="HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        byte[] bytes = mac.doFinal(data.getBytes("UTF8"));
        System.out.println(EncodeConvertUtils.toHex(bytes));




        return bytes;
    }

    public static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception {
        byte[] kSecret = ("AWS4" + key).getBytes("UTF8");
        System.out.println(kSecret);
        byte[] kDate = HmacSHA256(dateStamp, kSecret);
        byte[] kRegion = HmacSHA256(regionName, kDate);
        byte[] kService = HmacSHA256(serviceName, kRegion);
        byte[] kSigning = HmacSHA256("aws4_request", kService);
        return kSigning;
    }
    public static void main(String[] args) throws Exception {
//        byte[] signatureKey = getSignatureKey("wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY", "20120215", "us-east-1", "s3");
//        System.out.println(EncodeConvertUtils.toHex(signatureKey));
        String policy = "{ \"expiration\": \"2018-03-27T00:00:00Z\",\n" +
                "  \"conditions\": [\n" +
                "    {\"bucket\": \"jiachaotest2\"},\n" +
                "    [\"starts-with\", \"$key\", \"appinfo/\"],\n" +
                "    {\"acl\": \"public-read\"},\n" +
                "    {\"success_action_redirect\": \"https://jiachaotest2.s3.ap-southeast-1.amazonaws.com/successful_upload.html\"},\n" +
                "    [\"starts-with\", \"$Content-Type\", \"image/\"],\n" +
                "    [\"starts-with\", \"$x-amz-meta-tag\", \"\"]\n" +
                "  ]\n" +
                "}";
        getSigned(policy);
    }

    public static void getSigned(String policy_document) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        String policy = (new BASE64Encoder()).encode(
                policy_document.getBytes("UTF-8")).replaceAll("\n","").replaceAll("\r","");
        System.out.println(policy);
        Mac hmac = Mac.getInstance("HmacSHA1");
        hmac.init(new SecretKeySpec(
                aws_secret_key.getBytes("UTF-8"), "HmacSHA1"));
        String signature = (new BASE64Encoder()).encode(
                hmac.doFinal(policy.getBytes("UTF-8")))
                .replaceAll("\n", "");

        System.out.println(signature);
    }

}
