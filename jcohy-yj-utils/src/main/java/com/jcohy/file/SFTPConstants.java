package com.jcohy.file;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/11/11 10:36.
 * ClassName  : SFTPConstants
 * Description  :
 */
public class SFTPConstants {
    public static final String SFTP_REQ_HOST = "host";
    public static final String SFTP_REQ_PORT = "port";
    public static final String SFTP_REQ_USERNAME = "username";
    public static final String SFTP_REQ_PASSWORD = "password";
    public static final int SFTP_DEFAULT_PORT = 22;
    public static final String SFTP_REQ_LOC = "location";

    public static final int TIMEOUT=60000;

    public static final String DST = "/home/soft/"; // 目标文件名

    public static Map<String ,String> getDefaultSftp(){
        Map<String,String> map =new HashMap<String,String>();
        map.put(SFTPConstants.SFTP_REQ_HOST, "123.207.137.98");
        map.put(SFTPConstants.SFTP_REQ_USERNAME, "root");
        map.put(SFTPConstants.SFTP_REQ_PASSWORD, "jcoh1993");
        map.put(SFTPConstants.SFTP_REQ_PORT, "22");
        return map;
    }
}
