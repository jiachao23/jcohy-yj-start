package com.jcohy;

import com.jcohy.lang.StringUtils;

import java.util.Arrays;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/6 15:42.
 * ClassName  : TestString
 * Description  :
 */
public class TestString {


    public static void main(String[] args) {
        /**
         * StringUtils.isEmpty(null)      = true
         * StringUtils.isEmpty("")        = true
         * StringUtils.isEmpty(" ")       = false
         * StringUtils.isEmpty("bob")     = false
         * StringUtils.isEmpty("  bob  ") = false
         */
        System.out.println("------------------isEmpty------------------");
        System.out.println(StringUtils.isEmpty(null));
        System.out.println(StringUtils.isEmpty("") );
        System.out.println(StringUtils.isEmpty(" "));
        System.out.println(StringUtils.isEmpty("bob"));
        System.out.println(StringUtils.isEmpty("  bob  "));

        System.out.println("------------------isNotEmpty------------------");
        System.out.println(StringUtils.isNotEmpty(null));
        System.out.println(StringUtils.isNotEmpty("") );
        System.out.println(StringUtils.isNotEmpty(" "));
        System.out.println(StringUtils.isNotEmpty("bob"));
        System.out.println(StringUtils.isNotEmpty("  bob  "));

        /*
         * StringUtils.isBlank(null)      = true
         * StringUtils.isBlank("")        = true
         * StringUtils.isBlank(" ")       = true
         * StringUtils.isBlank("bob")     = false
         * StringUtils.isBlank("  bob  ") = false
         */
        System.out.println("------------------isBlank------------------");
        System.out.println(StringUtils.isBlank(null));
        System.out.println(StringUtils.isBlank("") );
        System.out.println(StringUtils.isBlank(" "));
        System.out.println(StringUtils.isBlank("bob"));
        System.out.println(StringUtils.isBlank("  bob  "));

        System.out.println("------------------isNotBlank------------------");
        System.out.println(StringUtils.isNotBlank(null));
        System.out.println(StringUtils.isNotBlank("") );
        System.out.println(StringUtils.isNotBlank(" "));
        System.out.println(StringUtils.isNotBlank("bob"));
        System.out.println(StringUtils.isNotBlank("  bob  "));

        /**
         * StringUtils.hashEmpty(null)             = true
         * StringUtils.hashEmpty(null, "foo")      = true
         * StringUtils.hashEmpty("", "bar")        = true
         * StringUtils.hashEmpty("bob", "")        = true
         * StringUtils.hashEmpty("  bob  ", null)  = true
         * StringUtils.hashEmpty(" ", "bar")       = false
         * StringUtils.hashEmpty("foo", "bar")     = false
         * StringUtils.hashEmpty(new String[]{})   = false
         * StringUtils.hashEmpty(new String[]{""}) = true
         */
        System.out.println("------------------hashEmpty------------------");
        System.out.println(StringUtils.hashEmpty(null));
        System.out.println(StringUtils.hashEmpty(null, "foo") );
        System.out.println(StringUtils.hashEmpty("", "bar"));
        System.out.println(StringUtils.hashEmpty("bob", ""));
        System.out.println(StringUtils.hashEmpty("  bob  ", null));
        System.out.println(StringUtils.hashEmpty(" ", "bar"));
        System.out.println(StringUtils.hashEmpty("foo", "bar"));
        System.out.println(StringUtils.hashEmpty(new String[]{}));
        System.out.println(StringUtils.hashEmpty(new String[]{""}));


        System.out.println("------------------isAllEmpty------------------");
        System.out.println(StringUtils.isAllEmpty(null));
        System.out.println(StringUtils.isAllEmpty(null, "foo") );
        System.out.println(StringUtils.isAllEmpty("", "bar"));
        System.out.println(StringUtils.isAllEmpty("bob", ""));
        System.out.println(StringUtils.isAllEmpty("  bob  ", null));
        System.out.println(StringUtils.isAllEmpty(" ", "bar"));
        System.out.println(StringUtils.isAllEmpty("foo", "bar"));
        System.out.println(StringUtils.isAllEmpty(new String[]{}));
        System.out.println(StringUtils.isAllEmpty(new String[]{""}));

        System.out.println("------------------hashBlank------------------");
        System.out.println(StringUtils.hashBlank(null));
        System.out.println(StringUtils.hashBlank(null, "foo") );
        System.out.println(StringUtils.hashBlank("", "bar"));
        System.out.println(StringUtils.hashBlank("bob", ""));
        System.out.println(StringUtils.hashBlank("  bob  ", null));
        System.out.println(StringUtils.hashBlank(" ", "bar"));
        System.out.println(StringUtils.hashBlank("foo", "bar"));
        System.out.println(StringUtils.hashBlank(new String[]{}));
        System.out.println(StringUtils.hashBlank(new String[]{""}));

        System.out.println("------------------isAllBlank------------------");
        System.out.println(StringUtils.isAllBlank(null));
        System.out.println(StringUtils.isAllBlank(null, "foo") );
        System.out.println(StringUtils.isAllBlank("", "bar"));
        System.out.println(StringUtils.isAllBlank("bob", ""));
        System.out.println(StringUtils.isAllBlank("  bob  ", null));
        System.out.println(StringUtils.isAllBlank(" ", "bar"));
        System.out.println(StringUtils.isAllBlank("foo", "bar"));
        System.out.println(StringUtils.isAllBlank(new String[]{}));
        System.out.println(StringUtils.isAllBlank(new String[]{""}));

        System.out.println("------------------cleanBlank------------------");
        System.out.println(StringUtils.cleanBlank(null));
        System.out.println(StringUtils.cleanBlank("") );
        System.out.println(StringUtils.cleanBlank(" "));
        System.out.println(StringUtils.cleanBlank("bob"));
        System.out.println(StringUtils.cleanBlank("  bob  "));

        /**
         * StringUtils.trim(null)          = null
         * StringUtils.trim("")            = ""
         * StringUtils.trim("     ")       = ""
         * StringUtils.trim("abc")         = "abc"
         * StringUtils.trim("    abc    ") = "abc"
         */
        System.out.println("------------------trim------------------");
        System.out.println(StringUtils.trim(StringUtils.trim(null)));
        System.out.println(StringUtils.trim(StringUtils.trim("")     ) );
        System.out.println(StringUtils.trim(StringUtils.trim("     ")));
        System.out.println(StringUtils.trim(StringUtils.trim("abc")));
        System.out.println(StringUtils.trim(StringUtils.trim("    abc    ")));
        System.out.println(StringUtils.trimStart("    start    "));
        System.out.println(StringUtils.trimEnd("    end    "));
        System.out.println(StringUtils.trimToEmpty(null));
        System.out.println(StringUtils.trimToNull("        "));

        System.out.println("------------------capitalize------------------");
        System.out.println(StringUtils.capitalize("cat"));
        System.out.println(StringUtils.upperCase("cat") );
        System.out.println(StringUtils.lowerCase("Cat"));
        System.out.println(StringUtils.isAllLowerCase("  cAt  "));
        System.out.println(StringUtils.isAllLowerCase("  cat  "));
        System.out.println(StringUtils.isAllLowerCase(StringUtils.cleanBlank("  cat  ")));
        System.out.println(StringUtils.isAllUpperCase("  cAt  "));
        System.out.println(StringUtils.isAllUpperCase("  CAT  "));
        System.out.println(StringUtils.isAllUpperCase(StringUtils.cleanBlank("  CAT  ")));
//
        System.out.println("------------------toUnderlineCase,toCamelCase------------------");
        System.out.println(StringUtils.toUnderlineCase("getName"));
        System.out.println(StringUtils.toCamelCase("get_name") );

        /**
         * StringUtils.split("a.b.c", '.')    = ["a", "b", "c"]
         * StringUtils.split("a..b.c", '.')   = ["a", "b", "c"]
         * StringUtils.split("a:b:c", '.')    = ["a:b:c"]
         * StringUtils.split("a b c", ' ')    = ["a", "b", "c"]
         */
        System.out.println("------------------split------------------");
        System.out.println(StringUtils.split("a b c", ' ')  );
        System.out.println(StringUtils.split("a.b.c", '.'));
        System.out.println(StringUtils.split("a..b.c", '.'));
        System.out.println(StringUtils.split("a:b:c", '.'));

        System.out.println(StringUtils.split("a:b:c", ':',2));
        System.out.println(Arrays.toString(StringUtils.splitToArray("a:b:c", ':')));
        System.out.println(Arrays.toString(StringUtils.splitToArray("a:b:c", ':', 2)));

        /**
         * * StringUtils.join(null, *)               = null
         * StringUtils.join([], *)                 = ""
         * StringUtils.join([null], *)             = ""
         * StringUtils.join([1, 2, 3], ';')  = "1;2;3"
         * StringUtils.join([1, 2, 3], null) = "123"
         */
        System.out.println("------------------join------------------");
        System.out.println(StringUtils.join(new String[]{"1","2","3"},';'));

        System.out.println("------------------deleteWhitespace------------------");
        System.out.println(StringUtils.deleteWhitespace("   ab  c  "));

        System.out.println("------------------remove------------------");
        System.out.println(StringUtils.remove("12122123","2"));
        System.out.println(StringUtils.removePattern("1212212sss3","[a-zA-Z]"));

        /**
         * StringUtils.substring("abc", 0, 2)   = "ab"
         * StringUtils.substring("abc", 2, 0)   = ""
         * StringUtils.substring("abc", 2, 4)   = "c"
         * StringUtils.substring("abc", 4, 6)   = ""
         * StringUtils.substring("abc", 2, 2)   = ""
         * StringUtils.substring("abc", -2, -1) = "b"
         * StringUtils.substring("abc", -4, 2)  = "ab"
         */

        System.out.println("------------------substring------------------");
        System.out.println(StringUtils.substring("abc", 0, 2));
        System.out.println(StringUtils.substring("abc", 2, 0));
        System.out.println(StringUtils.substring("abc", -2, -1));
        System.out.println(StringUtils.substring("abc", -4, 2));

        /**
         * StringUtils.overlay("abcdef", "zzzz", 2, 8)   = "abzzzz"
         * StringUtils.overlay("abcdef", "zzzz", -2, -3) = "zzzzabcdef"
         */

        System.out.println("------------------overlay------------------");
        System.out.println(StringUtils.overlay("abcdef", "zzzz", 2, 8));
        System.out.println(StringUtils.overlay("abcdef", "zzzz", -2, -3) );

        /**
         * StringUtils.chomp("abc\r\n")     = "abc"
         * StringUtils.chomp("abc\r\n\r\n") = "abc\r\n"
         * StringUtils.chomp("abc\n\r")     = "abc\n"
         * StringUtils.chomp("abc\n\rabc")  = "abc\n\rabc"
         */
        System.out.println("------------------chomp------------------");
        System.out.println(StringUtils.chomp("abc\r\n"));
        System.out.println(StringUtils.chomp("abc\r\n\r\n") );
        System.out.println(StringUtils.chomp("abc\n\r"));
        System.out.println(StringUtils.chomp("abc\n\rabc") );

        System.out.println("------------------repeat------------------");
        System.out.println(StringUtils.repeat("abc",5));
        System.out.println(StringUtils.repeat('d',5) );

        /**
         * StringUtils.countMatches("abba", "a")   = 2
         * StringUtils.countMatches("abba", "ab")  = 1
         */
        System.out.println("------------------countMatches------------------");
        System.out.println(StringUtils.countMatches("abba", "a") );
        System.out.println(StringUtils.countMatches("abba", "ab")  );


        /**
         * StringUtils.abbreviate("abcdefg", 6) = "abc..."
         * StringUtils.abbreviate("abcdefg", 7) = "abcdefg"
         */
        System.out.println("------------------countMatches------------------");
        System.out.println(StringUtils.abbreviate("abcdefg", 6) );
        System.out.println(StringUtils.abbreviate("abcdefg", 7)  );


    }
}
