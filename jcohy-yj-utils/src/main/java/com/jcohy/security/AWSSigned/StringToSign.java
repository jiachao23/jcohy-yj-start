package com.jcohy.security.AWSSigned;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/20 16:49.
 * ClassName  : StringToSign
 * Description  :
 */
public class StringToSign {

    private String algorithm;

    private String requestDateTime;

    private String credentialScope;

    private String regionName;

    private String serviceName;

    private String hashedCanonicalRequest;

    public String getRegionName() {
        return regionName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public StringToSign() {
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public String getCredentialScope() {
        return credentialScope;
    }

    public void setCredentialScope(String credentialScope) {


        this.credentialScope = credentialScope;
    }

    public String getHashedCanonicalRequest() {
        return hashedCanonicalRequest;
    }

    public void setHashedCanonicalRequest(String hashedCanonicalRequest) {
        this.hashedCanonicalRequest = hashedCanonicalRequest;
    }
}
