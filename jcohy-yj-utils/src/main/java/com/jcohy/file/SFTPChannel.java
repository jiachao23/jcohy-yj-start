package com.jcohy.file;

import com.jcraft.jsch.*;
//import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

/**
 * Copyright  : 2015-2033 Beijing Startimes Communication & Network Technology Co.Ltd
 * Created by jiac on 2017/11/11 10:15.
 * ClassName  : JschKit
 * Description  :
 */
public class SFTPChannel {

//    private static final Logger logger =LoggerFactory.getLogger(SFTPChannel.class);

    private Session session = null;

    private Channel channel =null;

    /**
     * 通过Map获取channel实例
     * @param sftpDetails
     * @param timeout
     * @return
     * @throws JSchException
     */
    public ChannelSftp getChannel(Map<String, String> sftpDetails, int timeout) throws JSchException {

        String ftpHost = sftpDetails.get(SFTPConstants.SFTP_REQ_HOST);
        String port = sftpDetails.get(SFTPConstants.SFTP_REQ_PORT);
        String ftpUserName = sftpDetails.get(SFTPConstants.SFTP_REQ_USERNAME);
        String ftpPassword = sftpDetails.get(SFTPConstants.SFTP_REQ_PASSWORD);
        int ftpPort = SFTPConstants.SFTP_DEFAULT_PORT;

        if (port != null && !port.equals("")) {
            ftpPort = Integer.valueOf(port);
        }

        JSch jsch = new JSch(); // 创建JSch对象
        session = jsch.getSession(ftpUserName, ftpHost, ftpPort);

//        logger.info("Session created.");// 根据用户名，主机ip，端口获取一个Session对象

        if (ftpPassword != null) {
            session.setPassword(ftpPassword); // 设置密码
        }
        Properties config = new Properties();

        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config); // 为Session对象设置properties
        session.setTimeout(timeout); // 设置timeout时间
        session.connect(); // 通过Session建立链接

//        logger.info("Session connected");
//        logger.info("Opening Channel.");

        channel = session.openChannel("sftp"); // 打开SFTP通道
        channel.connect(); // 建立SFTP通道的连接

//        logger.info("Connected successfully to ftpHost = " + ftpHost + ",as ftpUserName = " + ftpUserName
//                + ", returning: " + channel);
        return (ChannelSftp) channel;
    }

    /**
     *传入一个通道对象
     * @param username 远程要连接的服务器的用户名
     * @param password 远程要连接的服务器的密码
     * @param ip 远程服务器ip
     * @param port 远程服务器的ssh服务端口
     * @return ChannelSftp 返回指向这个通道指定的地址的channel实例
     * @throws JSchException
     */
    public  ChannelSftp getChannel(String username, String password, String ip, String port) throws JSchException {
        JSch jsch = new JSch(); // 创建JSch对象
        // 根据用户名，主机ip，端口获取一个Session对象
        session = jsch.getSession(username, ip, Integer.valueOf(port));

//        logger.info("Session created...");
        if (password != null) {
            session.setPassword(password); // 设置密码
        }
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config); // 为Session对象设置properties
        session.setTimeout(SFTPConstants.TIMEOUT); // 设置timeout时间
        session.connect(); // 通过Session建立链接
//        logger.info("Session connected, Opening Channel...");
        channel = session.openChannel("sftp"); // 打开SFTP通道
        channel.connect(); // 建立SFTP通道的连接
//        logger.info("Connected successfully to ip :{}, ftpUsername is :{}, return :{}",
//                ip,username, channel);
        return (ChannelSftp) channel;
    }

    /**
     * channel实例
     * @param sftpDetails
     * @return
     * @throws Exception
     */
    public  ChannelSftp getChannel(Map<String, String> sftpDetails) throws JSchException{
        return getChannel(sftpDetails, SFTPConstants.TIMEOUT);
    }

    public void closeChannel() throws Exception {
        if (channel != null) {
            channel.disconnect();
        }
        if (session != null) {
            session.disconnect();
        }
    }

}
