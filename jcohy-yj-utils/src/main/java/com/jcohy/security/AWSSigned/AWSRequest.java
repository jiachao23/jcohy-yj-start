package com.jcohy.security.AWSSigned;

import java.util.List;

/**
 * Copyright  : 2015-2033 Beijing
 * Created by jiac on 2018/3/20 15:40.
 * ClassName  : AWSRequest
 * Description  :
 */
public class AWSRequest {

    /**
     *  请求方法
     */

    private String requestMethod;

    /**
     *  规范请求URI
     */
    private String canonicalUri;

    /**
     *  查询字符串
     */
    private String canonicalQueryString ;

    /**
     *  规范标头
     */
    private List<CanonicalHeaders> canonicalHeaders;

    /**
     *  添加已签名的标头
     */
    private String signedHeaders;

    /**
     *  负载哈希值
     */
    private String hexEncode;

    public AWSRequest() {
    }

    public AWSRequest(String requestMethod, String canonicalUri, String canonicalQueryString, List<CanonicalHeaders> canonicalHeaders, String signedHeaders, String hexEncode) {
        this.requestMethod = requestMethod;
        this.canonicalUri = canonicalUri;
        this.canonicalQueryString = canonicalQueryString;
        this.canonicalHeaders = canonicalHeaders;
        this.signedHeaders = signedHeaders;
        this.hexEncode = hexEncode;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getCanonicalUri() {
        return canonicalUri;
    }

    public void setCanonicalUri(String canonicalUri) {
        this.canonicalUri = canonicalUri;
    }

    public String getCanonicalQueryString() {
        return canonicalQueryString;
    }

    public void setCanonicalQueryString(String canonicalQueryString) {
        this.canonicalQueryString = canonicalQueryString;
    }

    public List<CanonicalHeaders> getCanonicalHeaders() {
        return canonicalHeaders;
    }

    public void setCanonicalHeaders(List<CanonicalHeaders> canonicalHeaders) {
        this.canonicalHeaders = canonicalHeaders;
    }

    public String getSignedHeaders() {
        return signedHeaders;
    }

    public void setSignedHeaders(String signedHeaders) {
        this.signedHeaders = signedHeaders;
    }

    public String getHexEncode() {
        return hexEncode;
    }

    public void setHexEncode(String hexEncode) {
        this.hexEncode = hexEncode;
    }
}
